package com.socialuni.social.user.sdk.model.QO;

import lombok.Data;

@Data
public class LocationQueryQO {
    //ip
    private String ip;
    //纬度
    private String latitude;
    //经度
    private String longitude;
}
