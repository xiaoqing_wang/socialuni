export default class SocialuniRequestHeaderName {
    public static system = "system";
    public static platform = "platform";
    public static provider = "provider";
    public static socialuniCityAdCode = "socialuniCityAdCode";
    public static socialuniCityLon = "socialuniCityLon";
    public static socialuniCityLat = "socialuniCityLat";
}
